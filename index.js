module.exports = {
	name: 'Dependencies',

	// unused, but just for information to readers of the code
	author: 'notme',
	description: 'System for mods to wait for other mods to load.',

	extraScripts: [],

	config: {
		logMainOnly: true
	},

	init: function () {
		// nothing to load
		this.loaded(this);
	},

	// call loaded when your mod is loaded
	loaded: function (mod) {
		// mod info detection
		let info = {
			name: mod.name || 'Unknown',
			description: mod.description || mod.desc || 'N/A',
			author: mod.author || 'Unknown',
			extraScripts: ((mod.extraScripts || []).length > 0 ? mod.extraScripts.join(', ') : 'None')
		};
		
		this.log(`[Dependencies] Mod reported as loaded: ${info.name}
\tDescription: ${info.description},
\tAuthor: ${info.author}
\tExtra Scripts: ${info.extraScripts}`);

		// emit so mods can listen for it
		this.events.emit('dependencies:onModLoadedAny', mod);
		this.events.emit('dependencies:' + mod.name);
	},

	// logger
	log: function (...text) {
		let isMain = !process.argv[1].endsWith('worker');

		if (isMain) {
			// pass on to console.log
			console.log(...text);
		} else if (!this.config.logMainOnly) {
			// pass on to console.log if logging is allowed
			console.log(...text);
		}
	}
};
