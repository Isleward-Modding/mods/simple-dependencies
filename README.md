# Dependencies

*Mod by notme#1560*

*Last tested with Isleward v0.2.1*

A mod to make waiting for dependencies easier, at least until an actual way to wait for other mods to load is added to the core game.
Follow the installing instructions for setup or follow the usage section for how to use it in your mod.

## Installing

To add this mod, you just need to add it to the mods folder, but to have it work correctly, the folder should be named something like `_dependencies` so that it's at the beginning of the alphabetical order.
This is so that dependencies is loaded first, since other mods might need dependencies already loaded to function.

## Usage

To register your own mod to dependencies, just use a short bit of code like this:

```js
require('../_dependencies/index').loaded(this);
```

This just requires the dependencies mod and calls `.loaded()` with the mod.
Dependencies will then try to find the name, author, description, and other information from the mod, and logs a bit of information.
Then it fires some events, which other mods can hook in to.
If your mod needs to wait for another mod, you can hook these events as well.

Dependencies emits two events: `dependencies:onModLoadedAny`, which has whatever the mod passed in `.loaded()` in it's event data, and `dependencies:???` where `???` is the name property of the mod.
If you need to get when any mod is loaded, hook modLoadedAny, but if you need to wait for the mod 'abcdef' for example, you would listen for `dependencies:abcdef`.
